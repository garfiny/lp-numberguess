#!/bin/bash

docker build --force-rm -t activemq:latest -f Dockerfile.activemq .

docker run -p 8161:8161 -p 61616:61616 -td --name activemq --hostname=activemq activemq:latest
while true; do
    lastlog=$(docker logs --tail 5 activemq)
    if [[ $lastlog == *"ActiveMQ WebConsole available"* ]]; then
        break
    else
        sleep 5
    fi
done
echo "ActiveMQ is ready"
