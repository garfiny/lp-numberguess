## Build & Deploy

### number guess web app

* go to numberguss directory
* run - mvn clean install -DskipTests=true
* run - cp target/numberguess-0.0.1-SNAPSHOT.jar ../servers/


### number guess client app

* go to numbergussclient directory
* run - ./gradlew build or gradle build if you have gradle installed on your local machine
* run - cp build/libs/numberguessclient-0.0.1-SNAPSHOT.jar ../servers/

### build docker containers

* run - docker-compose build
* run - docker-compose up
