package com.shuozhao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import java.util.Random;

@SpringBootApplication
public class NumberguessclientApplication {

	private static final int SEED = 10;
	private static final int BOUND = 100;
	private static final long INTERVAL = 10000;

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	public static void main(String[] args) {
		ConfigurableApplicationContext context =
				SpringApplication.run(NumberguessclientApplication.class, args);
		RestTemplate restTemplate = context.getBean(RestTemplate.class);
		String guessUrl = context.getEnvironment().getProperty("spring.app.url");
		Random random = new Random(SEED);
		while(true) {
			try {
				String url = guessUrl + random.nextInt(BOUND);
				restTemplate.put(url, null);
			} catch (Exception e) {
			} finally {
                try {
				    Thread.sleep(INTERVAL);
                }catch(InterruptedException ix) {}
            }
		}
	}
}
