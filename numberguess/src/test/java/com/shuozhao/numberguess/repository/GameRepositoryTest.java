package com.shuozhao.numberguess.repository;

import com.shuozhao.numberguess.domain.Game;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class GameRepositoryTest {

    @Autowired
    private GameRepository repository;

    @Test
    public void testFindByIsActiveTrue() throws Exception {
        Game game = new Game();
        game.setActive(true);
        game.setNumber(10);
        repository.save(game);
        Game game1 = repository.findByIsActiveTrue();
        assertTrue(game1.isActive());
        assertEquals(10, game1.getNumber());
    }

}