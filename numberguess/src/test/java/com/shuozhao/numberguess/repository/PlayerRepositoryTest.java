package com.shuozhao.numberguess.repository;

import com.shuozhao.numberguess.domain.Player;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class PlayerRepositoryTest {

    @Autowired
    private PlayerRepository repository;

    private static final String USERNAME_FOO = "foo";

    @Before
    public void setup() {
        Player player = new Player();
        player.setPoints(100);
        player.setComputer(false);
        player.setPassword("");
        player.setUsername(USERNAME_FOO);
        repository.save(player);
    }

    @Test
    public void testFindByUsername() throws Exception {
        Player foo = repository.findByUsername(USERNAME_FOO);
        assertEquals(USERNAME_FOO, foo.getUsername());
    }

    @Test
    public void findByIsComputerTrue() throws Exception {
        Player player = new Player();
        player.setPoints(100);
        player.setComputer(true);
        player.setPassword("");
        player.setUsername("user1");
        repository.save(player);
        Player computer = repository.findByIsComputerTrue();
        assertTrue(computer.isComputer());
    }
}