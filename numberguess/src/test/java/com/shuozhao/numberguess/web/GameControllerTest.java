package com.shuozhao.numberguess.web;

import com.shuozhao.numberguess.domain.Game;
import com.shuozhao.numberguess.domain.Player;
import com.shuozhao.numberguess.service.GameService;
import com.shuozhao.numberguess.service.GuessResult;
import com.shuozhao.numberguess.service.PlayerService;
import com.shuozhao.numberguess.utils.Guess;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(GameController.class)
public class GameControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private GameService gameService;

    @MockBean
    private PlayerService playerService;
    private Game game;
    private Player player;
    private Guess guess;

    @Before
    public void setup() {
        game = new Game();
        game.setId(1L);
        game.setActive(true);
        game.setNumber(10);
        player = new Player();
        player.setUsername("player1");
        player.setId(1L);
        guess = new Guess();
        guess.setNumber(game.getNumber());
        guess.setGameId(game.getId());
        guess.setPlayerId(player.getId());
    }

    @Test
    @WithMockUser
    public void testGuessWithWinningGuess() throws Exception {
        given(this.gameService.findById(anyInt())).willReturn(game);
        given(this.gameService.guess(anyObject())).willReturn(GuessResult.BINGO);
        given(this.playerService.getLoggedInPlayer()).willReturn(player);

        this.mvc.perform(post("/guess", guess))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testGuessWithWrongGuess() throws Exception {
        Optional<GuessResult> result =
                Arrays.stream(GuessResult.values()).filter(
                        value -> value != GuessResult.BINGO).findFirst();
        given(this.gameService.findById(anyInt())).willReturn(game);
        given(this.playerService.getLoggedInPlayer()).willReturn(player);
        given(this.gameService.guess(anyObject())).willReturn(GuessResult.BINGO);
        this.mvc.perform(post("/guess", guess))
                .andExpect(status().is3xxRedirection());
    }
}