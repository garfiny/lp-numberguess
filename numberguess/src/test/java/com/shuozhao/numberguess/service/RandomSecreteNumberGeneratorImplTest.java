package com.shuozhao.numberguess.service;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RandomSecreteNumberGeneratorImplTest {

    @Autowired
    private SecreteNumberGenerator generator;

    @Test
    public void testGeneratedNumberGreaterEqualToZero() {
        int number = generator.generate();
        assertThat(number, greaterThanOrEqualTo(0));
    }

    @Test
    public void testGeneratedNumberLessThanOrEqualTo100() {
        int number = generator.generate();
        assertThat(number, lessThanOrEqualTo(100));
    }
}