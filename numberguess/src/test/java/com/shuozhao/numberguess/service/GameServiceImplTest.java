package com.shuozhao.numberguess.service;

import com.shuozhao.numberguess.domain.Game;
import com.shuozhao.numberguess.domain.Player;
import com.shuozhao.numberguess.repository.GameRepository;
import com.shuozhao.numberguess.utils.Guess;
import com.shuozhao.numberguess.utils.RegistrationDetails;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class GameServiceImplTest {

    @Autowired
    private GameService service;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private PlayerService playerService;

    private Player player1;
    private Player player2;
    private Player computer;

    @Before
    public void setup() throws DuplicatePlayerNameException {
        player1 = playerService.register(new RegistrationDetails("user1", ""));
        player2 = playerService.register(new RegistrationDetails("user2", ""));
        computer = playerService.register(new RegistrationDetails("computer", ""));
        computer.setComputer(true);
        playerService.updatePlayer(computer);
    }

    @Test
    public void testStartNewGame() {
        Game game = service.startNewGame();
        assertNotNull(game);
        assertTrue(game.isActive());
    }

    @Test
    public void testFinishGame() {
        Game game = service.startNewGame();
        assertTrue(game.isActive());
        service.finishGame(game);
        Game game1 = service.findById(game.getId());
        assertFalse(game1.isActive());
    }

    @Test
    public void testCurrentGameWhenThereIsActiveGame() {
        service.startNewGame();
        Game game = service.currentGame();
        assertNotNull(game);
        assertTrue(game.isActive());
    }

    @Test
    public void testCurrentGameWhenThereIsNoActiveGame() {
        Game game = service.currentGame();
        assertNull(game);
    }

    @Test
    public void testFindById() {
        Game game = service.startNewGame();
        Game game1 = service.findById(game.getId());
        assertEquals(game.getNumber(), game1.getNumber());
        assertEquals(game.getId(), game1.getId());
    }

    @Test
    public void testGuessWhenNoActiveGame() {
        Game game = service.startNewGame();
        game.setActive(false);
        gameRepository.save(game);
        Guess guess = new Guess();
        guess.setGameId(game.getId());
        guess.setPlayerId(player1.getId());
        guess.setNumber(game.getNumber() - 1);
        GuessResult result = service.guess(guess);
        assertEquals(GuessResult.GAME_FINISHED, result);
    }

    @Test
    public void testGuessForMultiplePlayers() {
        Game game = service.startNewGame();
        Guess guess = new Guess();
        guess.setGameId(game.getId());
        guess.setPlayerId(player1.getId());
        guess.setNumber(game.getNumber() - 1);
        Guess guess1 = new Guess();
        guess1.setGameId(game.getId());
        guess1.setPlayerId(player2.getId());
        guess1.setNumber(game.getNumber() + 1);
        GuessResult result = service.guess(guess);
        GuessResult result1 = service.guess(guess1);
        assertEquals(GuessResult.TOO_FEW, result);
        assertEquals(GuessResult.TOO_HIGH, result1);
    }

    public void testGuessWhenGuessedNumberIsSmallerThanGameNumber() {
        Game game = service.startNewGame();
        Guess guess = new Guess();
        guess.setGameId(game.getId());
        guess.setPlayerId(player1.getId());
        guess.setNumber(game.getNumber() - 1);
        GuessResult result = service.guess(guess);
        assertEquals(GuessResult.TOO_FEW, result);
    }

    @Test
    public void testGuessWhenGuessedNumberIsLargerThanGameNumber() {
        Game game = service.startNewGame();
        Guess guess = new Guess();
        guess.setGameId(game.getId());
        guess.setPlayerId(player1.getId());
        guess.setNumber(game.getNumber() + 1);
        GuessResult result = service.guess(guess);
        assertEquals(GuessResult.TOO_HIGH, result);
    }

    public void testGuessWhenSubmitTooOftenAsHumanPlayer() {
        Game game = service.startNewGame();
        Guess guess = new Guess();
        guess.setGameId(game.getId());
        guess.setPlayerId(player1.getId());
        guess.setNumber(randomNumber(game));
        Guess guess1 = new Guess();
        guess1.setGameId(game.getId());
        guess1.setPlayerId(player1.getId());
        guess1.setNumber(randomNumber(game));
        GuessResult result = service.guess(guess);
        GuessResult result1 = service.guess(guess1);
        assertEquals(GuessResult.SUBMIT_TOO_FREQUENT, result1);
    }

    @Test
    public void testGuessWhenSubmitTooOftenAsComputerPlayer() throws InterruptedException {
        Game game = service.startNewGame();
        Guess guess = new Guess();
        guess.setGameId(game.getId());
        guess.setPlayerId(computer.getId());
        guess.setNumber(randomNumber(game));
        Guess guess1 = new Guess();
        guess1.setGameId(game.getId());
        guess1.setPlayerId(computer.getId());
        guess1.setNumber(randomNumber(game));
        GuessResult result = service.guess(guess);
        Thread.sleep(1100);
        GuessResult result1 = service.guess(guess1);
        assertEquals(GuessResult.SUBMIT_TOO_FREQUENT, result1);
    }

    @Test
    public void testGuessWhenWinningTheGame() {
        Game game = service.startNewGame();
        Guess guess = new Guess();
        guess.setGameId(game.getId());
        guess.setPlayerId(player1.getId());
        guess.setNumber(game.getNumber());
        GuessResult result = service.guess(guess);
        assertEquals(GuessResult.BINGO, result);
    }

    private int randomNumber(Game game) {
        Random random = new Random();
        int randomNumber = random.nextInt(100);
        if (game == null) return randomNumber;
        while(game.getNumber() == randomNumber) {
            randomNumber = random.nextInt();
        }
        return randomNumber;
    }
}