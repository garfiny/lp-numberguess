package com.shuozhao.numberguess.service;

import com.shuozhao.numberguess.domain.Player;
import com.shuozhao.numberguess.utils.RegistrationDetails;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class PlayerServiceImplTest {

    @Autowired
    private PlayerService service;

    private static final RegistrationDetails existingPlayer =
            new RegistrationDetails("Foo", "password");

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() throws DuplicatePlayerNameException {
        service.register(existingPlayer);
    }

    @Test
    public void testRegisterNewPlayer() throws DuplicatePlayerNameException {
        String playerName = "Bar";
        RegistrationDetails newPlayer = new RegistrationDetails(playerName, "password");
        Player player = service.register(newPlayer);
        assertEquals(playerName, player.getUsername());
        assertFalse(player.isComputer());
    }

    @Test
    public void testRegisterExistingPlayer() throws DuplicatePlayerNameException {
        thrown.expect(DuplicatePlayerNameException.class);
        service.register(existingPlayer);
    }

    @Test
    public void testAutoLogin() {
        service.autologin(existingPlayer.getUsername(), existingPlayer.getPassword());
        assertTrue(SecurityContextHolder.getContext().getAuthentication().isAuthenticated());
    }

    @Test
    public void testGetLoggedInPlayerWhenSuccessfullyLoggedIn() {
        service.autologin(existingPlayer.getUsername(), existingPlayer.getPassword());
        Player player = service.getLoggedInPlayer();
        assertEquals(existingPlayer.getUsername(), player.getUsername());
    }

    @Test
    public void testFindAllPlayers() {
        List<Player> allPlayers = service.findAllPlayers();
        assertEquals(1, allPlayers.size());
    }

    @Test
    public void testFindById() {
        service.autologin(existingPlayer.getUsername(), existingPlayer.getPassword());
        Player player = service.getLoggedInPlayer();
        Player player1 = service.findById(player.getId());
        assertEquals(player.getId(), player1.getId());
        assertEquals(player.getUsername(), player1.getUsername());
    }

    @Test
    public void testUpdatePlayer() {
        service.autologin(existingPlayer.getUsername(), existingPlayer.getPassword());
        Player player = service.getLoggedInPlayer();
        player.setPoints(50);
        service.updatePlayer(player);
        Player player1 = service.findById(player.getId());
        assertEquals(50, player1.getPoints());
    }
}