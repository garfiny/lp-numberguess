package com.shuozhao.numberguess.service;

import com.shuozhao.numberguess.domain.Game;
import com.shuozhao.numberguess.domain.Player;
import com.shuozhao.numberguess.repository.GameRepository;
import com.shuozhao.numberguess.repository.PlayerRepository;
import com.shuozhao.numberguess.utils.Guess;
import com.shuozhao.numberguess.utils.RegistrationDetails;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import java.util.Random;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class GuessMessageListenerTest {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private GameService gameService;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private PlayerRepository playerRepository;

    private Player player;
    private Game game;

    @Before
    public void setup() throws DuplicatePlayerNameException {
        player = playerService.register(new RegistrationDetails("user1", ""));
        game = gameService.startNewGame();
    }

    private void sendMessage(Guess guess) {
        jmsTemplate.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createObjectMessage(guess);
            }
        });
    }

    private Guess newGuess() {
        Guess guess = new Guess();
        guess.setPlayerId(player.getId());
        guess.setGameId(game.getId());
        guess.setNumber(new Random().nextInt(100));
        return guess;
    }

    @Test
    public void testMessageWithInActiveGame() {
        game.setActive(false);
        gameRepository.save(game);
        int points = player.getPoints();
        sendMessage(newGuess());
        Player player1 = playerRepository.findOne(player.getId());
        assertEquals(points, player1.getPoints());
    }

    @Test
    public void testMessageWithWinningGuess() throws InterruptedException {
        Guess guess = newGuess();
        guess.setNumber(game.getNumber());
        int points = player.getPoints() + game.getNumber();
        sendMessage(guess);
        Thread.sleep(100);
        Player player1 = playerRepository.findOne(player.getId());
        assertEquals(points, player1.getPoints());
    }

    @Test
    public void testMessageWithWrongGuess() throws InterruptedException {
        Guess guess = newGuess();
        guess.setNumber(game.getNumber() + 1);
        int points = player.getPoints();
        sendMessage(guess);
        Thread.sleep(100);
        Player player1 = playerRepository.findOne(player.getId());
        assertEquals(points - 1, player1.getPoints());
    }

}