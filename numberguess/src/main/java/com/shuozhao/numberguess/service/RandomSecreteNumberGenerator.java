package com.shuozhao.numberguess.service;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class RandomSecreteNumberGenerator implements SecreteNumberGenerator {

    @Override
    public int generate() {
        return new Random().nextInt(101);
    }
}
