package com.shuozhao.numberguess.service;

public enum GuessResult {

    BINGO, TOO_HIGH, TOO_FEW, SUBMIT_TOO_FREQUENT, GAME_FINISHED, INPUT_NOT_VALID
}
