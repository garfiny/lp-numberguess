package com.shuozhao.numberguess.service;

import com.shuozhao.numberguess.domain.Game;
import com.shuozhao.numberguess.domain.Player;
import com.shuozhao.numberguess.utils.Guess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

@Component
public class GuessMessageListener implements MessageListener {

    static final Logger LOG = LoggerFactory.getLogger(GuessMessageListener.class);


    @Autowired
    private GameService gameService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private MessageConverter messageConverter;

    @Override
    public void onMessage(Message message) {
        try {
            processGuessMessage(message);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    private void processGuessMessage(Message message) throws JMSException {
        Guess guess = (Guess) messageConverter.fromMessage(message);
        LOG.debug("Received message: " + guess.toString());
        Game game = gameService.findById(guess.getGameId());
        if (!game.isActive()) return;

        Player player = playerService.findById(guess.getPlayerId());
        if (game.getNumber() == guess.getNumber()) {
            player.setPoints(player.getPoints() + game.getNumber());
            gameService.finishGame(game);
        } else {
            player.setPoints(player.getPoints() - 1);
        }
        playerService.updatePlayer(player);
    }

}
