package com.shuozhao.numberguess.service;

import com.shuozhao.numberguess.domain.Game;
import com.shuozhao.numberguess.domain.Player;
import com.shuozhao.numberguess.repository.GameRepository;
import com.shuozhao.numberguess.utils.Guess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class GameServiceImpl implements GameService {

    @Autowired
    private SecreteNumberGenerator generator;

    @Autowired
    private GameRepository repository;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private PlayerService playerService;

    private Map<Long, Long> lastGuesses = new HashMap<>();

    @Override
    public Game startNewGame() {
        Game game = new Game();
        game.setNumber(generator.generate());
        game.setActive(true);
        repository.save(game);
        return game;
    }

    @Override
    @Transactional
    public void finishGame(Game game) {
        game.setActive(false);
        repository.save(game);
    }

    @Override
    public Game currentGame() {
        return repository.findByIsActiveTrue();
    }

    @Override
    public Game findById(long id) {
        return repository.findOne(id);
    }

    @Override
    public GuessResult guess(Guess guess) {
        Game game = repository.findOne(guess.getGameId());
        if (!game.isActive()) {
            return GuessResult.GAME_FINISHED;
        }
        if (guessTooFrequent(guess)) return GuessResult.SUBMIT_TOO_FREQUENT;

        jmsTemplate.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createObjectMessage(guess);
            }
        });
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }

        if (game.getNumber() == guess.getNumber()) {
            return GuessResult.BINGO;
        } else if (game.getNumber() > guess.getNumber()) {
            return GuessResult.TOO_FEW;
        } else if (game.getNumber() < guess.getNumber()) {
            return GuessResult.TOO_HIGH;
        }
        return GuessResult.INPUT_NOT_VALID;
    }

    private boolean guessTooFrequent(Guess guess) {
        Player player = playerService.findById(guess.getPlayerId());
        long now = System.currentTimeMillis();
        if (lastGuesses.containsKey(guess.getPlayerId())) {
            long lastReceivedTimestamp = lastGuesses.get(guess.getPlayerId());
            lastGuesses.put(guess.getPlayerId(), now);
            long freqThreshold = player.isComputer() ? COMPUTER_QUESSING_FREQ : HUMAN_QUESSING_FREQ;
            return now - lastReceivedTimestamp < freqThreshold;
        } else {
            lastGuesses.put(guess.getPlayerId(), now);
            return false;
        }
    }
}
