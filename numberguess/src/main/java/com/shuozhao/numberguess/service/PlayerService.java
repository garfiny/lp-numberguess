package com.shuozhao.numberguess.service;

import com.shuozhao.numberguess.domain.Player;
import com.shuozhao.numberguess.utils.RegistrationDetails;

import java.util.List;

public interface PlayerService {

    public static final int INITIAL_PLAY_POINTS = 100;

    Player register(RegistrationDetails user) throws DuplicatePlayerNameException;

    Player getLoggedInPlayer();

    void autologin(String username, String password);

    List<Player> findAllPlayers();

    Player findById(Long id);

    void updatePlayer(Player player);
}
