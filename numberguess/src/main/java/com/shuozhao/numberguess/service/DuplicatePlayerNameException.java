package com.shuozhao.numberguess.service;

public class DuplicatePlayerNameException extends Exception {
    public DuplicatePlayerNameException(String msg) {
        super(msg);
    }
}
