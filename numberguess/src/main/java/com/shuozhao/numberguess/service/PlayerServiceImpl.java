package com.shuozhao.numberguess.service;

import com.shuozhao.numberguess.domain.Player;
import com.shuozhao.numberguess.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import com.shuozhao.numberguess.utils.RegistrationDetails;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PlayerServiceImpl implements PlayerService {

    @Autowired
    private PlayerRepository repository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    public Player register(RegistrationDetails user) throws DuplicatePlayerNameException {
        Player player = repository.findByUsername(user.getUsername());
        if (player != null) {
            throw new DuplicatePlayerNameException("Player " + user.getUsername() + " has been registered!");
        }
        Player newPlayer = new Player();
        newPlayer.setUsername(user.getUsername());
        newPlayer.setPassword(user.getPassword());
        newPlayer.setPoints(INITIAL_PLAY_POINTS);
        newPlayer.setComputer(false);
        return repository.save(newPlayer);
    }

    @Override
    public Player getLoggedInPlayer() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return repository.findByUsername(authentication.getName());
    }

    @Override
    public void autologin(String username, String password) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(
                        userDetails, password,
                        userDetails.getAuthorities());

        authenticationManager.authenticate(usernamePasswordAuthenticationToken);

        if (usernamePasswordAuthenticationToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        }
    }

    public List<Player> findAllPlayers() {
        return repository.findAll();
    }

    public Player findById(Long id) { return repository.findOne(id); }

    @Transactional
    public void updatePlayer(Player player) {
        repository.save(player);
    }
}
