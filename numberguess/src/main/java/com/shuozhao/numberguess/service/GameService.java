package com.shuozhao.numberguess.service;

import com.shuozhao.numberguess.domain.Game;
import com.shuozhao.numberguess.utils.Guess;

public interface GameService {

    static final long COMPUTER_QUESSING_FREQ = 10000;
    static final long HUMAN_QUESSING_FREQ = 1000;

    Game startNewGame();

    void finishGame(Game game);

    Game currentGame();

    Game findById(long id);

    GuessResult guess(Guess guess);
}
