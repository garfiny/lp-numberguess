package com.shuozhao.numberguess.utils;

import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.*;
import java.io.Serializable;

public class Guess implements Serializable {

    @NotNull
    @Min(0)
    @Max(100)
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private int number;
    private long gameId;
    private long playerId;

    public long getGameId() {
        return gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    @Override
    public String toString() {
        return "Guess{" +
                "number=" + number +
                ", gameId=" + gameId +
                ", playerId=" + playerId +
                '}';
    }
}
