package com.shuozhao.numberguess.utils;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class RegistrationDetails {

    @NotNull
    @NotEmpty
    private String username;

    @NotNull
    private String password;

    public RegistrationDetails() { }

    public RegistrationDetails(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
