package com.shuozhao.numberguess;

import com.shuozhao.numberguess.domain.Game;
import com.shuozhao.numberguess.service.GameService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
public class NumberguessApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext applicationContext = SpringApplication.run(NumberguessApplication.class, args);
		GameService gameService = applicationContext.getBean(GameService.class);
		Game game = gameService.currentGame();
		if (game == null) gameService.startNewGame();
	}
}
