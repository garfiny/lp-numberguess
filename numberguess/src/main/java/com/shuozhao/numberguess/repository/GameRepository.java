package com.shuozhao.numberguess.repository;

import com.shuozhao.numberguess.domain.Game;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameRepository extends JpaRepository<Game, Long> {

    Game findByIsActiveTrue();
}
