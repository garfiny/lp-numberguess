package com.shuozhao.numberguess.repository;

import com.shuozhao.numberguess.domain.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long>{

    Player findByUsername(String username);

    Player findByIsComputerTrue();
}
