package com.shuozhao.numberguess.web;

import com.shuozhao.numberguess.domain.Game;
import com.shuozhao.numberguess.domain.Player;
import com.shuozhao.numberguess.service.DuplicatePlayerNameException;
import com.shuozhao.numberguess.service.GameService;
import com.shuozhao.numberguess.service.PlayerService;
import com.shuozhao.numberguess.utils.Guess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.shuozhao.numberguess.utils.RegistrationDetails;
import org.springframework.web.servlet.ModelAndView;
import javax.validation.Valid;

@Controller
public class HomeController {

    @Autowired
    private PlayerService playerService;

    @Autowired
    private GameService gameService;

    @RequestMapping("/")
    public String index(Model model) {
        Game game = gameService.currentGame();
        if (game == null) game = gameService.startNewGame();
        model.addAttribute("game", game);
        model.addAttribute("players", playerService.findAllPlayers());
        model.addAttribute("guess", new Guess());
        return "index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(Model model) {
        return new ModelAndView("login", "loginDetails", new Player());
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView registration() {
        return new ModelAndView("registration","registrationDetails",
                new RegistrationDetails());
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String register(@Valid RegistrationDetails registrationDetails,
                           BindingResult bindingResult, Errors errors) {

        if (!bindingResult.hasErrors()) {
            try {
                playerService.register(registrationDetails);
                playerService.autologin(registrationDetails.getUsername(), registrationDetails.getPassword());
            } catch (DuplicatePlayerNameException e) {
                bindingResult.rejectValue("username", "message.duplicatePlayerName");
            }
        }
        if (bindingResult.hasErrors()) {
            return "registration";
        } else {
            return "redirect:/";
        }
    }
}
