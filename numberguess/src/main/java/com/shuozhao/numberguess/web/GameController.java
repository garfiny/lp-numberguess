package com.shuozhao.numberguess.web;

import com.shuozhao.numberguess.domain.Game;
import com.shuozhao.numberguess.domain.Player;
import com.shuozhao.numberguess.service.GameService;
import com.shuozhao.numberguess.service.GuessResult;
import com.shuozhao.numberguess.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.shuozhao.numberguess.utils.Guess;
import static com.shuozhao.numberguess.service.GuessResult.*;

import javax.validation.Valid;

@Controller
public class GameController {

    @Autowired
    private GameService gameService;

    @Autowired
    private PlayerService playerService;

    @RequestMapping("/winner")
    public String winner(long gameId, Model model) {
        model.addAttribute("game", gameService.findById(gameId));
        model.addAttribute("player", playerService.getLoggedInPlayer());
        return "winner";
    }

    @RequestMapping(value = "/guess", method = RequestMethod.POST)
    public ModelAndView guess(@Valid Guess guess,
                BindingResult bindingResult, Model model) {

        Game game = gameService.findById(guess.getGameId());
        Player player = playerService.getLoggedInPlayer();

        if (!game.isActive()) {
            return new ModelAndView("error", "message", "The game #" + game.getId() + " has finished!");
        }
        guess.setPlayerId(player.getId());
        GuessResult result = gameService.guess(guess);

        if (result == BINGO) {
            return new ModelAndView("redirect:/winner", "gameId", game.getId());
        } else {
            switch (result) {
                case SUBMIT_TOO_FREQUENT:
                    bindingResult.rejectValue("number", "message.guess.too_often");
                case TOO_FEW:
                    bindingResult.rejectValue("number", "message.guess.too_few");
                    break;
                case TOO_HIGH:
                    bindingResult.rejectValue("number", "message.guess.too_high");
                    break;
                case INPUT_NOT_VALID:
                    bindingResult.rejectValue("number", "message.guess.number");
                    break;
            }
            model.addAttribute("game", game);
            model.addAttribute("players", playerService.findAllPlayers());
            model.addAttribute("guess", guess);
            return new ModelAndView("index");
        }
    }
}
