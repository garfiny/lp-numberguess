package com.shuozhao.numberguess.web;

import com.shuozhao.numberguess.domain.Game;
import com.shuozhao.numberguess.domain.Player;
import com.shuozhao.numberguess.repository.PlayerRepository;
import com.shuozhao.numberguess.service.GameService;
import com.shuozhao.numberguess.utils.Guess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ComputerInterfaceController {

    static final Logger LOG = LoggerFactory.getLogger(ComputerInterfaceController.class);

    @Autowired
    private GameService gameService;

    @Autowired
    private PlayerRepository playerRepository;

    @RequestMapping(value = "/computer/{number}", method = RequestMethod.PUT)
    public ResponseEntity<?> guess(@PathVariable int number) {
        Player computer = playerRepository.findByIsComputerTrue();
        if (computer == null) {
            computer = new Player();
            computer.setPoints(100);
            computer.setUsername("computer");
            computer.setComputer(true);
            computer.setPassword("");
            playerRepository.save(computer);
        }
        LOG.debug("Received PUT Request from: " + computer.getId());

        Game game = gameService.currentGame();
        Guess guess = new Guess();
        guess.setNumber(number);
        guess.setGameId(game.getId());
        guess.setPlayerId(computer.getId());
        gameService.guess(guess);
        return ResponseEntity.ok("ok");
    }
}
