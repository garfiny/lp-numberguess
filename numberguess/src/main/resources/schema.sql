CREATE TABLE IF NOT EXISTS game (
    id INTEGER PRIMARY KEY,
    number INTEGER NOT NULL,
    is_active CHAR(1) NOT NULL
);

CREATE TABLE IF NOT EXISTS player (
    id INTEGER PRIMARY KEY,
    username CHAR(100) NOT NULL,
    password CHAR(100),
    points INTEGER NOT NULL,
    is_computer CHAR(1)
);
